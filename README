README: Python Podcast Feed Generator
=====================================

Python Podcast Feed Generator is a Flask/Python interface that allows a user to
maintain an arbitrary number of podcasts and serve an RSS feed that podcatchers 
can subscribe to. It does so relying on the `python-feedgen`[1] module.

For installation instructions, see `INSTALL`. For pending tasks and known bugs
see `TODO`.

This application is my first attempt to create a multi-module Python project and
therefore it must not be relied upon for any purpose. I am simply scratching an
itch and forcing myself to improve my coding skills. At it's current stage, 
there are no performance optimisations, its security is untested, and it only 
offers a barebone HTML interface without any styling or UX considerations. 
Nevertheless, as of its first tagged revision, it already implements all basic 
functions I have planned. 

Feedback on how to improve the code are welcome. Ideas about new features are 
also welcome but do keep in mind that this application should not be considered
as 'actively maintained' for purposes of deploying it in a public-facing way.

Of course, it is free/libre software under the terms of the GNU Affero General 
Public License version 3 or (at your option) any later version[2]. By respecting
the license terms, you can modify the code to fit your needs. I would appreciate
a heads-up if you do that.

For the list of contributors see `AUTHORS`. For non-contributor acknowledgments,
see `ACKNOWLEDGEMENTS`.


[1]: https://feedgen.kiesow.be/index.html
[2]: https://www.gnu.org/licenses/agpl-3.0.html

* * *

    python-podcast-feed-generator provides a simple web interface to feedgen
    Copyright (C) 2020  Demetris Karayiannis dkarayiannis.eu/p/contact

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/.
