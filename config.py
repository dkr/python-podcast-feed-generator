#!/usr/bin/env python3
'''
    python-podcast-feed-generator provides a simple web interface to feedgen
    config.py stores app configuration
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

import os

# Set basedir to be the present location
basedir = os.path.abspath(os.path.dirname(__file__))
# Set datadir to be $basedir/data
datadir = basedir + "/data"


class Config(object):
    # SQLAlchemy Database Path configuration - ENV or default value
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'sqlite:///' + os.path.join(datadir, 'podcast_data.db')
    # Toggle Track Modifications as needed
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # Secret key for cryptographic operations - always change this via ENV or
    # here as a hardcoded string
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'YOU MUST CHANGE ME!'
