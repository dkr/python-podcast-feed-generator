#!/usr/bin/env python3
'''
    python-podcast-feed-generator provides a simple web interface to feedgen
    app/feeds.py handles feed generation
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from feedgen.feed import FeedGenerator
from app.helpers import aware_utctime


# Generation code

# =============================================================================
# def feedgen(podcast: dict):
#     fg = FeedGenerator()
#     fg.title(podcast['title'])
#     fg.author(podcast['author'])
#     fg.description(podcast['subtitle'])
#     fg.link(href=podcast['link'], rel='alternate')
#     fg.logo(podcast['logo'])
#     fg.language(podcast['language'])
#
#     for entry, value in podcast['entries'].items():
#         fe = fg.add_entry()
#         fe.id(value['guid'])
#         fe.title(value['title'])
#         fe.pubDate(value['pubDate'])
#         try:
#             fe.description(value['description'])
#         except AttributeError:  # obsolete, DB prevents it
#             fe.description(value['title'])
#         fe.enclosure(url=value['enclosure']['url'],
#                      length=value['enclosure']['length'],
#                      type=value['enclosure']['type'])
#     return fg
# =============================================================================

def feedgendb(podcast, episodes):
    fg = FeedGenerator()
    fg.title(podcast.title)
    fg.author({
                'name': podcast.author_name, 'email': podcast.author_email
                })
    fg.description(podcast.subtitle)
    fg.link(href=podcast.link, rel='alternate')
    fg.logo(podcast.logo)
    fg.language(podcast.language)

    for episode in episodes:
        fe = fg.add_entry()
        fe.id(episode.guid)
        fe.title(episode.title)
        fe.pubDate(aware_utctime(episode.pubDate))  # Always UTC!
        try:
            fe.description(episode.description)
        except AttributeError:  # obsolete, DB model prevents it
            fe.description(episode.title)
        fe.enclosure(url=episode.enc_url,
                     length=episode.enc_length,  # FIXME Make it str in DB
                     type=episode.enc_type)
    return fg
