#!/usr/bin/env python3
'''
    python-podcast-feed-generator provides a simple web interface to feedgen
    app/helpers.py contains helper functions
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from datetime import datetime
from pytz import timezone


def aware_utcnow():
    '''
    >>> aware_utcnow()
    datetime.datetime(2020, 12, 27, 22, 54, 57, 220450, tzinfo=<UTC
    '''
    return timezone('UTC').localize(datetime.utcnow())


def aware_utctime(t):
    '''
    >>> aware_utctime(2020, 12, 30, 1, 13, 50, 310142)
    datetime.datetime(2020, 12, 30, 1, 13, 50, 310142, tzinfo=<UTC>)
    '''
    return timezone('UTC').localize(t)


def datetime_merge(d, t):
    '''
    >>> datetimeMerge(d, t)
    datetime.datetime(2020, 12, 30, 1, 13, 50, 310142, tzinfo=<UTC>)
    '''
    dt = datetime.combine(d, t)
    return dt
