#!/usr/bin/env python3
'''
    python-podcast-feed-generator provides a simple web interface to feedgen
    app/routes.py defines the routes
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from app import app, db
from app.models import Podcast, Episode, User
from app.forms import (LoginForm, RegistrationForm, EditPodcastForm,
                       EditEpisodeForm)
from app.feeds import feedgendb
from app.helpers import aware_utctime, datetime_merge, aware_utcnow
from flask_login import login_user, current_user, logout_user, login_required
from flask import (render_template, flash, redirect, Response, request,
                   url_for)


@app.route('/')
@app.route('/index')
# TODO: Template and content are currently just a placeholder
def index():
    with app.open_resource('../README', 'r') as f:
        readme = f.read().replace('\n', '<br>')
    return render_template('index.html', readme=readme)


@app.route('/login', methods=['GET', 'POST'])
def login():
    '''
    If the current user is not authenticated, we ask for username and password.
    If the username doesn't exist or the password hash doesn't match the user's
    stored password hash, the login fails.
    TODO: Obligatory TOTP
    '''
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Login failed. Check username and password.')
            return redirect(url_for('login'))
        else:
            login_user(user, remember=form.remember_me.data)
            flash('User "{}" succesfully logged in.'.format(user.username))
            next_page = request.args.get('next')
            return (redirect(next_page) if next_page
                    else redirect(url_for('index')))
    return render_template('login.html', title='Log In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    '''
    We only allow one user to ever be registered, since this is a single-user
    app. We check if there is any user in the DB. If not, we present a
    registration form.
    '''
    any_user = User.query.count()
    if any_user != 0:
        flash(f"This is a single user application and a user already exists. "
              f"For instructions about recovering your account, consult the "
              f"documentation.")
        return redirect(url_for('index'))
    else:
        form = RegistrationForm()
        flash('First Run: Admin User Creation.')
        if form.validate_on_submit():
            user = User(username=form.username.data)
            user.set_password(form.password.data)
            db.session.add(user)
            db.session.commit()
            flash("Admin user account created!")
            return redirect(url_for('login'))
        return render_template('register.html', title='Register', form=form)


@app.route('/pod')
# We list existing podcasts here.
# TODO: Add /pod/<podcast> with an episode list, and /pod/<podcast>/edit
def list_podcasts():
    return render_template('podcast_list.html', podcasts=Podcast.query.all())


@app.route('/pod/add', methods=['GET', 'POST'])
@login_required
def add_podcast():
    '''
    This page presents a form requesting all data fields requiried to create
    a database query to add a new Podcast().
    '''
    form = EditPodcastForm()
    if form.validate_on_submit():
        '''
        If there are no validation errors, we submit data to DB session and
        commit them.
        '''
        podcast = Podcast(
                            title=form.title.data,
                            author_name=form.author_name.data,
                            author_email=form.author_email.data,
                            link=form.link.data,
                            logo=form.logo.data,
                            language=form.language.data,
                            subtitle=form.subtitle.data,
                            )
        flash('The submitted data for "{}" was valid.'.format(
                form.title.data))
        db.session.add(podcast)
        db.session.commit()
        flash('The new entry for "{}" was saved.'.format(
                form.title.data))
        return redirect(url_for('list_podcasts'))  # maybe redir to /pod/<id>
    else:
        if form.errors.items():
            '''
            If any of the form fields doesn't validate, we flash a warning.
            The app/template/podcast_form.html contains logic to print the
            validation errors for each field.
            '''
            flash(f"The data provided contained one or more errors and "
                  f"could not be submitted. Consult the error messages below "
                  f"revise.")
    return render_template('podcast_form.html', title='Add Podcast...',
                           form=form)


@app.route('/pod/<int:pod_id>')
def list_episodes(pod_id):
    podcast = Podcast.query.get(pod_id)
    episodes = Episode.query.filter(Episode.pod_id == pod_id)
    # FIXME: perf, https://docs.sqlalchemy.org/en/13/orm/query.html?highlight=count#sqlalchemy.orm.query.Query.count
    ep_count = episodes.count()
    return render_template('podcast_details.html', podcast=podcast,
                           num_of_episodes=ep_count,
                           episodes=episodes
                           )


@app.route('/pod/<int:pod_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_podcast(pod_id):
    current_podcast = Podcast.query.filter_by(id=pod_id).first()
    '''
    This page presents a form requesting all data fields that can be modified
    in a Podcast. It reuses add_podcast().
    '''
    form = EditPodcastForm()
    if form.submit():
        '''
        We assign current title to read-only field in form, to use in
        comparison to new title and skip title uniqueness if old == new
        '''
        form.db_idx.data = current_podcast.id
    if form.validate_on_submit():
        '''
        If there are no validation errors, we submit data to DB session and
        commit them.
        '''
        current_podcast.title = form.title.data
        current_podcast.author_name = form.author_name.data
        current_podcast.author_email = form.author_email.data
        current_podcast.link = form.link.data
        current_podcast.logo = form.logo.data
        current_podcast.language = form.language.data
        current_podcast.subtitle = form.subtitle.data
        flash('The submitted data for "{}" was valid.'.format(
                form.title.data))
        db.session.commit()
        flash('The updated entry for "{}" was saved.'.format(
                form.title.data))
        return redirect(url_for('list_podcasts'))  # maybe redir to /pod/<id>
    elif request.method == 'GET':
        form.db_idx.data = current_podcast.id
        form.title.data = current_podcast.title
        form.subtitle.data = current_podcast.subtitle
        form.author_name.data = current_podcast.author_name
        form.author_email.data = current_podcast.author_email
        form.link.data = current_podcast.link
        form.logo.data = current_podcast.logo
        form.language.data = current_podcast.language
    else:
        if form.errors.items():
            '''
            If any of the form fields doesn't validate, we flash a warning.
            The app/template/podcast_form.html contains logic to print the
            validation errors for each field.
            '''
            flash(f"The data provided contained one or more errors and "
                  f"could not be submitted. Consult the error messages below "
                  f"revise.")
    return render_template('podcast_form.html',
                           title='Editing: {}'.format(current_podcast.title),
                           form=form)


@app.route('/pod/<int:pod_id>/add', methods=['GET', 'POST'])
@login_required
def add_episode(pod_id):
    '''
    This page presents a form requesting all data fields requiried to create
    a database query to add a new Episode.
    '''
    podcast = Podcast.query.get(pod_id)
    form = EditEpisodeForm()
    if form.validate_on_submit():
        '''
        If there are no validation errors, we submit data to DB session and
        commit them.
        '''
        try:
            dt = aware_utctime(datetime_merge(
                    form.pubDate.date.data, form.pubDate.time.data))
        except TypeError:
            dt = aware_utcnow()
        episode = Episode(
                guid=form.guid.data,
                title=form.title.data,
                description=form.description.data,
                # pubDate returns date, time seperately. First merge into one
                # datetime object, then make it tz-aware as UTC
                pubDate=dt,
                enc_url=form.enc_url.data,
                enc_length=form.enc_length.data,  # FIXME in models.py
                enc_type=form.enc_type.data,
                pod_id=podcast.id
                )
        flash('The submitted data for "{}" was valid.'.format(form.title.data))
        db.session.add(episode)
        db.session.commit()
        flash('The new entry for "{}" was saved.'.format(
                form.title.data))
        flash(form.enc_length.type)
        return redirect(url_for('list_episodes', pod_id=podcast.id))
    else:
        if form.errors.items():
            '''
            If any of the form fields doesn't validate, we flash a warning.
            The app/template/podcast_form.html contains logic to print the
            validation errors for each field.
            '''
            flash(f"The data provided contained one or more errors and "
                  f"could not be submitted. Consult the error messages below "
                  f"revise.")
    return render_template('episode_form.html',
                           title='Add Episode to {}'.format(podcast.title),
                           form=form)


@app.route('/pod/<int:pod_id>/<int:episode_id>')
def episode_details(pod_id, episode_id):
    podcast = Podcast.query.get(pod_id)
    episode = Episode.query.get(episode_id)
    return render_template('episode_details.html', podcast=podcast,
                           episode=episode
                           )


@app.route('/pod/<int:pod_id>/<int:episode_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_episode(pod_id, episode_id):
    '''
    This page presents a form requesting all data fields that can be modified
    in a Podcast.
    '''
    podcast = Podcast.query.get(pod_id)  # FIXME rm to current_podcast
    episode = Episode.query.get(episode_id)  # FIXME rm to current_episode
    form = EditEpisodeForm()
    if form.validate_on_submit():
        '''
        If there are no validation errors, we submit data to DB session and
        commit them.
        '''
        try:
            dt = aware_utctime(datetime_merge(
                    form.pubDate.date.data, form.pubDate.time.data))
        except TypeError:
            dt = aware_utcnow()
        episode.guid = form.guid.data
        episode.title = form.title.data
        episode.description = form.description.data
        # pubDate returns date, time seperately. First merge into one
        # datetime object, then make it tz-aware as UTC
        episode.pubDate = dt
        episode.enc_url = form.enc_url.data
        episode.enc_length = form.enc_length.data  # why is this not int
        episode.enc_type = form.enc_type.data
        episode.pod_id = podcast.id
        flash('The submitted data for "{}" was valid.'.format(form.title.data))
        db.session.commit()  # TODO: Stash multiple changes and ask to Save
        flash('The updated entry for "{}" was saved.'.format(
                form.title.data))
        return redirect(url_for('list_episodes', pod_id=podcast.id))
    elif request.method == 'GET':
        form.guid.data = episode.guid
        form.title.data = episode.title
        form.description.data = episode.description
        form.pubDate.date.data = episode.pubDate.date()
        form.pubDate.time.data = episode.pubDate.time()
        form.enc_url.data = episode.enc_url
        form.enc_length.data = episode.enc_length
        form.enc_type.data = episode.enc_type
    else:
        if form.errors.items():
            '''
            If any of the form fields doesn't validate, we flash a warning.
            The app/template/episode.html contains logic to print the
            validation errors for each field.
            '''
            flash(f"The data provided contained one or more errors and "
                  f"could not be submitted. Consult the error messages below "
                  f"revise.")
    return render_template('episode_form.html',
                           title='Editing: {} [{}] ({})'.format(
                                   episode.title, episode.id, podcast.title),
                           form=form)


@app.route('/pod/<int:pod_id>.rss')
def serve_feed(pod_id):
    # FIXME: This re-generates on each request - it shouldn't be used in
    # the public facing part. Implement a serve_feed(), keep this for a
    # preview function
    podcast = Podcast.query.get(pod_id)
    episodes = Episode.query.filter(Episode.pod_id == pod_id)
    podcast_feed = feedgendb(podcast, episodes)
    return Response(podcast_feed.rss_str(pretty=True), mimetype='text/xml')


@app.route('/generate_all_feeds', methods=['POST', 'GET'])
def generate_all_feeds():
    # FIXME: this should be a loop for podcast in podcasts: generate_feed()
    # Depends on first implementing feed generation to public static xml
    flash('Not implemented')
    return redirect(url_for('index'))
