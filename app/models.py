#!/usr/bin/env python3
'''
    python-podcast-feed-generator provides a simple web interface to feedgen
    app/models.py defines the database structure
    Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

from app import db, login_manager
from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


@login_manager.user_loader
def get_user(user_id):
    '''
    Get user by User.id
    '''
    return User.query.get(int(user_id))


class Podcast(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, unique=True, nullable=False)
    author_name = db.Column(db.String, nullable=False)
    author_email = db.Column(db.String, nullable=False)
    link = db.Column(db.String, nullable=False)
    logo = db.Column(db.String, nullable=False, default='default.jpg')
    # https://www.rssboard.org/rss-language-codes
    language = db.Column(db.String, nullable=False, default='en')
    subtitle = db.Column(db.Text, nullable=False)
    entries = db.relationship('Episode', backref='show', lazy=True)

    def __repr__(self):
        return (
                f"Podcast(title={self.title}, author_name={self.author_name}, "
                f"author_email={self.author_email}, link={self.link}, "
                f"logo={self.logo}, language={self.language}, "
                f"subtitle={self.subtitle})"
            )


class Episode(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    guid = db.Column(db.String, nullable=False)
    title = db.Column(db.String, nullable=False)
    pubDate = db.Column(db.DateTime, nullable=False, index=True,
                        default=datetime.utcnow)
    description = db.Column(db.Text, nullable=False)
    enc_url = db.Column(db.String, nullable=False)
    enc_length = db.Column(db.String, nullable=False, default='0')
    enc_type = db.Column(db.String, nullable=False, default='audio/mpeg')
    pod_id = db.Column(db.Integer, db.ForeignKey('podcast.id'), nullable=False)

    def __repr__(self):
        return (
                f"Episode(guid={self.guid}, title={self.title}, "
                f"pubDate={self.pubDate}, description={self.description}, "
                f"enc_url={self.enc_url}, enc_length={self.enc_length}, "
                f"enc_type={self.enc_type}, pod_id={self.pod_id})"
            )


class User(UserMixin, db.Model):
    # Keep in mind that this app is not intended to be multi-user
    # Login is only used for access control edit functions
    # TODO: Obligatory TOTP
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(64))
    # otp_secret = db.Column(db.String(16))

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return (
                f"User(username={self.username}, password={self.password_hash})"
                )
