#!/usr/bin/env python3
'''
    python-podcast-feed-generator provides a simple web interface to feedgen
    Copyright (C) 2020  Demetris Karayiannis <dkarayiannis.eu/p/contact>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from app import app, db
from app.models import Podcast, Episode, User


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Podcast': Podcast, 'Episode': Episode, 'User': User}
